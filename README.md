# direct_cv_encode


# opencv-ffmpeg
Converting opencv image to ffmpeg image

This code can be used to record video from default camera and then converting Mat image to AVFrame.
Try changing the value of "NUM_FRAMES" in the code to record videos of different durations.

Added b Eric Bachard (2019 / 07 / 06)

Project goal modified in : directly encode cv::Mat into .mp4 using ffmpeg 

You'll need ffmpeg 4.0.x + dependencies to compile this code.

